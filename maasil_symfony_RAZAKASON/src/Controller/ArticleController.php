<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Entity\Auteur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface; 
class ArticleController extends AbstractController
{
    /**
     * @Route("/", name="article")
     */
    public function index(ArticleRepository $article_repo): Response
    {
        $article = $article_repo->findBy(
            array(),
            array('id' => 'DESC'),
            5
        );
        return $this->render('article/index.html.twig', [
            'controller_name' => 'ArticleController',
            'articles' => $article
        ]);
    }

    /**
     * @Route("/article/details/{id}",name="details_article")
     */
    public function details(Article $article){


        return $this->render("article/details.html.twig",[
            'article' => $article
        ]);
    }

    /**
     * @Route("/article/create_article", name="create_article")
     */
    public function createArticle(Request $request, EntityManagerInterface $manager) : Response{
        
        //verifiez l'utilisateur en cours
        $user = $this->getUser();
        $article = new Article();        
        $form_article = null;   

        //si il n'y a pas d'utilisateur connecter , on aura des choix d'utilisateurs dans la creation d'article
        if(empty($user)){
        $form_article = $this->createFormBuilder($article)
                             ->add("titre")
                             ->add("auteur",EntityType::class,[
                                 'class' => Auteur::class,
                                 'choice_label' => 'nom'
                             ])
                             ->add("texte")
                             ->getForm();
         }

         //mais si non l'article sera publier par l'utilisateur connecter
         else{
            $form_article = $this->createFormBuilder($article)
            ->add("titre")
            ->add("texte")
            ->getForm();
         }
         $form_article -> handleRequest($request);  

         if($form_article->isSubmitted() && $form_article->isValid()){
         
            if(!empty($user)){
                $article ->setDateDeCreation( new \DateTime())
                         ->setAuteur($user);
                $manager->persist( $article);
                $manager->flush();
                return $this->redirectToRoute("article");
            }
            else{
                
            $article ->setDateDeCreation( new \DateTime());
            $manager->persist( $article);
            $manager->flush();
            return $this->redirectToRoute("article");
            }

         }                
        return $this->render("article/create.html.twig",[
            'form_article' => $form_article->createView()
        ]);
    }
}
