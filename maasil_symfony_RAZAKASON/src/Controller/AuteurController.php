<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface; 
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use App\Entity\Auteur;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
class AuteurController extends AbstractController
{
    /**
     * @Route("/auteur", name="auteur")
     */
    public function index(): Response
    {
        return $this->render('auteur/index.html.twig', [
            'controller_name' => 'AuteurController',
        ]);
    }

    /**
     * @Route("/auteur/create",name="create_auteur")
     */

    public function create(Request $request, EntityManagerInterface $manager,UserPasswordEncoderInterface $encoder){

        $auteur = new Auteur();
        $form_auteur = $this->createFormBuilder($auteur)
                            ->add("nom")
                            ->add("password",PasswordType::class)
                            ->getForm();

        $form_auteur->handleRequest($request);
        if($form_auteur->isSubmitted() && $form_auteur->isValid()){
            $hash = $encoder -> encodePassword($auteur,$auteur->getPassword());
            $auteur -> setPassword($hash);
            $manager->persist($auteur);
            $manager->flush();
            $this->redirectToRoute("article");
        }

        return $this->render("auteur/create.html.twig",[
           'form_auteur' =>  $form_auteur->createView()
        ]);
    }

    /**
     * @Route("/login",name="login")
     */
    public function login(){
        return $this-> render("auteur/login.html.twig");
    }


    /**
     * @Route("/logout",name="logout")
     */
    public function logout(){

    }
}
