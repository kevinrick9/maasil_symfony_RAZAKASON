<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface; 
use App\Entity\Auteur;
use App\Entity\Article;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Creez un faux auteur
        $mdp = '$RhzppkToDITbKpfcyDEj8.';
        $auteur = new Auteur();
        $auteur-> setNom("Razakason")
               -> setPassword("$2y$13".$mdp."NXuCONl7KDwcKHIlG7kEmPd0On7IrF."); // mot de passe generer avec encode password designant "kevin"
        $manager->persist($auteur);

        // Creez un faux article;

        $article = new Article();
        $article->setTitre("Le 2 février 2021 au Japon : un « setsubun » pas comme les autres")
                ->setTexte("À l’origine, le setsubun est un rite de changement de saison. De nos jours, la date de cette coutume est fixée la veille du début du printemps (appelé risshun), au cours de laquelle des haricots de soja sont lancés contre les esprits maléfiques oni, les symboles des épidémies, pour les éliminer. Et s’il y a une année où ces vœux d’éradication se font pressants, c’est bien celle-ci... Le setsubun 2021 a un goût très spécial, d’autant plus que le calendrier est un peu modifié.")
                ->setDateDeCreation(new \DateTime)
                ->setAuteur($auteur);
        $manager->persist($article);


        $manager->flush();
    }
}
